<?php
    require_once("animal.php");
    require_once("Ape.php");
    require_once("Frog.php");

    $sheep = new Animal("shaun");
    echo "<b>Name</b> : $sheep->name <br>";
    echo "<b>Legs</b> : $sheep->legs <br>";
    echo "<b>Cold Blooded</b> : $sheep->cold_blooded <br><br>";

    $sungokong = new Ape("kera sakti");
    echo "<b>Name</b> : $sungokong->name <br>";
    echo "<b>Legs</b> : $sungokong->legs <br>";
    echo "<b>Cold Blooded</b> : $sungokong->cold_blooded <br>";
    echo "<b>Yell</b> : ";
    $sungokong->yell();
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "<b>Name</b> : $kodok->name <br>";
    echo "<b>Legs</b> : $kodok->legs <br>";
    echo "<b>Cold Blooded</b> : $kodok->cold_blooded <br>";
    echo "<b>Jump</b> : ";
    $kodok->jump();
    echo "<br><br>";
